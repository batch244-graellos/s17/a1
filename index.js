/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:
	let fullName = prompt("What is your complete name?");
	console.log("Hello, " + fullName);

	let age = prompt("How old are you?");
	console.log("You are " + age + " years old");

	let address = prompt("Where do you live?")
	console.log("You live in " + address);

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBand(){
		console.log('1. Planetshakers');
		console.log('2. Hillsong');
		console.log('3. Elevation Worship');
		console.log('4. Bamboo');
		console.log('5. Mayonaise');
	};

	favBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myFavMovies(){
		console.log("1. Avengers: End Game" );
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("2. Spiderman: No Way Home");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("3. Black Panther (2018)");
		console.log("Rotten Tomatoes Rating: 96%");
		console.log ("4. Fast Five");
		console.log("Rotten Tomatoes Rating: 86%");
		console.log ("5. Extraction");
		console.log("Rotten Tomatoes Rating: 70%%");
	};
		myFavMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

/*printUsers()*/; // Results in an error, much like variables, we cannot invoke a function we have yet to define. 


	alert("Hi! Please add the names of your friends.");

	function printUsers() {
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: " + friend1);
	console.log(friend2);
	console.log(friend3);
};

printUsers();